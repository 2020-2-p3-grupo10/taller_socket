#include "../include/cabecera.h"


int  crear_socket(){
	int sockfd=socket(AF_INET,SOCK_STREAM,0);
	if(sockfd<0){
		perror("Error al crear el socket");
		exit(1);
	}
	return sockfd;
}

int crear_servidor(int socket,int argc,char **argv){
	char opcion;
	unsigned short puerto;
	char *ipe,*p;
	while((opcion=getopt(argc,argv,"i:p:"))!=-1){
		switch(opcion){
			case 'i':
				ipe=optarg;
				break;
			case 'p':
				p=optarg;
				break;
			default:
				break;
		}
	}

	struct sockaddr_in direccion;
	direccion.sin_family = AF_INET;
	puerto=(unsigned short)atoi(p);
	direccion.sin_addr.s_addr=inet_addr(ipe);
	direccion.sin_port = htons(puerto);
	
	int res=bind(socket,(struct sockaddr *)&direccion,sizeof(direccion));//VINCULACION DE NUESTRO SERVIDOR
	if(res<0){//VALIDACION
		perror("Fallor la vinculacion del programa");
		close(socket);
		exit(1);
	}
	
	res=listen(socket,TAM);//SERVIDOR ESCUCHANDOO
 	if(res>0){ //VALIDACION
 		perror("Fallo el listen");
		close(socket);
		exit(1);
 	}
 	printf("SERVIDOR ESCUCHANDO .... \n");
 	return res;
}





