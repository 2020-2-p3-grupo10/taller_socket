#include "../include/cabecera.h"

int main(int argc, char **argv){
	char opcion;
	unsigned short puerto;
	char *ipe;
	char  *p;
	char *ruta;
	char *local;
	while((opcion=getopt(argc,argv,"i:p:R:L:"))!=-1){
		switch(opcion){
			case 'i':
				ipe=optarg;
				break;
			case 'p':
				p=optarg;
				break;
			case 'R':
				ruta=optarg;
				break;
			case 'L':
				local=optarg;
				break;
			default:
				break;
		}
	}	
        int sockfd=socket(AF_INET,SOCK_STREAM,0);
        if(sockfd<0){
                perror("Error al crear el socket");
                exit(1);
        }

	struct sockaddr_in direccion;
	direccion.sin_family = AF_INET;
	puerto=(unsigned short)atoi(p);
	direccion.sin_addr.s_addr=inet_addr(ipe);
	direccion.sin_port = htons(puerto);
	//CLIENTE
	int res=connect(sockfd,(struct sockaddr *) &direccion,sizeof(direccion));
	
	if(res<0){//VALIDACION
		perror("Fallor el connect");
		close(sockfd);
		exit(1);
	}
	printf("CONEXION ESTABLECIDA\n");
	//ENVIAR Y RECIBIR INFORMACION

	res= write(sockfd,ruta,strlen(ruta));
	if(res<0){//VALIDACION
		perror("FALLOR EL ENVIO DEL msj del CLIENTE");
		close(sockfd);
		exit(1);
	}
	char buf2[1000]={0};
	umask(0);
	int archivo=open(local,O_WRONLY | O_CREAT | O_TRUNC,0666);
	if (archivo<0){//VALIDACION
		perror("Error al crear el nuevo archivo");
		close(archivo);
		close(sockfd);
		exit(1);
	}
	while((res=read(sockfd,buf2,1000)) !=0){
		if(res<0){//VALIDACION
			perror("Error al leer archivo");
			close(sockfd);
			close(archivo);
			break;
		}
		int dat=write(archivo,buf2,res);
		if(dat<0){//VALIDACION
			perror("Error al escribir los bytes copiados en el archivo");
			close(sockfd);
			close(archivo);
			break;
		}
		memset(buf2,0,1000);
	}
	printf("ARCHIVO DESCARGADO\n");
 	close(sockfd);       
}
