#include "../include/cabecera.h"
//#include "funciones.c"
int main(int argc, char **argv){

	int sockfd=crear_socket();
	int srv=crear_servidor(sockfd,argc,argv);
	
	while(1){
	//SOCKET PAIR
		char buf[100]={0};
		int sockfd_connect = accept(sockfd,NULL,0);
		if(sockfd_connect<0){ //VALIDACION
			perror("Fallo  la conexcion al socket del cliente");
			close(sockfd);
			close(sockfd_connect);
			exit(1);
		}

	//ENVIO Y RECIBIR INFORMACION
		srv=read(sockfd_connect,buf,100);
		if(srv<0){ //VALIDACION
			perror("Fallo al leer el socket del cliente");
			close(sockfd_connect);
			exit(1);
		}
	// ABRIR LA IMAGEN DESCARGADA  
		char datos[1000]={0};
		int img=open(buf,O_RDONLY);
		if(img<0){ //VALIDACION
			perror("Error al abrir el archivo");
			close(sockfd_connect);
			exit(1);
		}
	//LEER DE 1000 BYTES EN 1000 BYTES todo EL ARCHIVO
		int dato;
		while((dato=read(img,datos,1000)) != 0){
			if (dato<0){ //VALIDACION
				perror("Error al leer la imagen");
				close(sockfd_connect);
				close(img);
				break;
			}
	//ENVIO LOS DATOS AL CLIENTE Y VACIO EL BUFFER PARA VOLVER OTRA VEZ AL PROCESO
			int datos_enviados=write(sockfd_connect,datos,dato);
			memset(datos,0,1000);
			if(datos_enviados<0){ //VALIDACION
				perror("Fallo el envio de bytes de la imagen");
				close(sockfd_connect);
				close(img);
				break;
			}
		}
		close(sockfd_connect);
	}
	close(sockfd);	
}

