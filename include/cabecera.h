#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // para cerrar procesos como close
#include <sys/socket.h> // para crear un socket,listen,accept
#include <netinet/in.h> // para struct sockaddr_int
#include <arpa/inet.h> // para convertir la direcciones de red
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#define TAM 3


int crear_socket();
int crear_servidor(int socket,int argc,char **argv);

