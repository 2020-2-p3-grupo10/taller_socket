all: bin/servidor bin/cliente

bin/servidor: obj/servidor.o obj/funciones.o
	gcc obj/servidor.o obj/funciones.o -o bin/servidor

bin/cliente: obj/cliente.o
	gcc obj/cliente.o -o bin/cliente


obj/cliente.o: src/cliente.c 
	gcc -Wall -c  -I include/  src/cliente.c -o obj/cliente.o

obj/servidor.o : src/servidor.c 
	gcc -Wall -c -I include/ src/servidor.c  -o obj/servidor.o

obj/funciones.o: src/funciones.c
	gcc -Wall -c  src/funciones.c -o obj/funciones.o

.PHONY: clean
clean: 
	rm -rf bin/* obj/*
